﻿using UnityEngine;
using System.Collections;

public class aerial_view : MonoBehaviour {

    private const int MAP_X_MIN = 660;
    private const int MAP_X_MAX = 1260;
    private const int MAP_Y_MIN = 380;
    private const int MAP_Y_MAX = 980;
    private const int MAP_LENGTH_X = 600;
    private const int MAP_LENGTH_Y = 600;
    private const float MAP_FACTOR = 0.5f;

    private Logic logic;

    private Waypoints waypoints;
    public GameObject camera_main;
    public GameObject camera_aerial;
    public GameObject camera_profile;

    void Start()
    {
        logic = GameObject.Find("Main Camera").GetComponent<Logic>();
        waypoints = GameObject.Find("Main Camera").GetComponent<Waypoints>();
    }

    void OnEnable()
    {
        camera_profile.SetActive(false);
        camera_aerial.SetActive(true);
    }
	
	void Update () {
        //Vector3 mouse = Input.mousePosition;
        //print("mouse x: " +  mouse.x.ToString() + "  y: " + mouse.y.ToString());
    }

    public void clickAerial()
    {
        Vector3 mouse = Input.mousePosition;

        if (mouse.x > MAP_X_MIN && mouse.x < MAP_X_MAX && mouse.y > MAP_Y_MIN && mouse.y < MAP_Y_MAX)
        {
            waypoints.waypoints.Add(new Vector3((-(MAP_Y_MIN + (MAP_LENGTH_X / 2) - mouse.y) * MAP_FACTOR), (MAP_X_MIN + (MAP_LENGTH_Y / 2) - mouse.x) * MAP_FACTOR, 100));
        }
    }
}
