﻿using UnityEngine;
using System.Collections;

public class drone : MonoBehaviour
{
    private const float MAP_FACTOR = 80;

    RosCommSandbox ros_comm;
    GameObject drone_;

    private Vector3 initialPosition;
    private Quaternion rotation;

    void Start()
    {
        ros_comm = GameObject.Find("Main Camera").GetComponent<RosCommSandbox>();
        drone_ = GameObject.Find("drone");
        initialPosition = drone_.transform.position;
    }

    void Update()
    {
        drone_.transform.position = new Vector3(initialPosition.x + ros_comm.packet_recv.t_x / MAP_FACTOR, initialPosition.y - ros_comm.packet_recv.t_y / MAP_FACTOR, initialPosition.z + ros_comm.packet_recv.t_z / MAP_FACTOR);
        Quaternion flipRotation = new Quaternion(ros_comm.packet_recv.q_x, -ros_comm.packet_recv.q_y, ros_comm.packet_recv.q_z, ros_comm.packet_recv.q_w);
        flipRotation = Quaternion.Inverse(flipRotation);
        flipRotation *= Quaternion.Euler(0, 0, 90);
        drone_.transform.rotation = flipRotation;
    }
}
