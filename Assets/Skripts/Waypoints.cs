﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using System.Threading;


public class Waypoints : MonoBehaviour {

    private const int LOAD_TIME = 30;
    private const int SEND_RATE = 20; //msgs per sec
    private static int load_timer = 0;

    private Logic logic;
    private InstantGuiPopup load_pu;
    private RosCommSandbox roscomm;
    private Particle particle;
    private Status status;

    private XmlSerializer mySerializer;
    private Thread waypoints_thread;
    private TouchScreenKeyboard keyboard;
    private float velocity;
    private float distance;
    private float duration;

    public List<Vector3> waypoints;
    public InstantGuiElement vel;
    public InstantGuiElement dis;
    public InstantGuiElement dur;
    public InstantGuiSlider speed;

    void Start () {
        logic = GameObject.Find("Main Camera").GetComponent<Logic>();
        load_pu = GameObject.Find("load_waypoints_pu").GetComponent<InstantGuiPopup>();
        mySerializer = new XmlSerializer(typeof(List<Vector3>));
        roscomm = GameObject.Find("Main Camera").GetComponent<RosCommSandbox>();
        particle = GameObject.Find("Particle System").GetComponent<Particle>();
        status = GameObject.Find("info_panel").GetComponent<Status>();

        waypoints = new List<Vector3>();
    }

    void Update ()
    {
        velocity = speed.value;
        distance = particle.distance;
        duration = distance / velocity;
        vel.text = velocity.ToString("n2");
        dis.text = distance.ToString("n2");
        dur.text = duration.ToString("n2");

        load_timer++;
        if(load_timer == LOAD_TIME)
        {
            string[] list_labels = load_pu.list.labels;
            string[] files = Directory.GetFiles("C:/Users/sebastian/Desktop/settings/waypoints");
            int length = files.Length;
            list_labels = new string[length];

            for (int counter = 0; counter < length; counter++)
            {
                list_labels[counter] = files[counter].Substring(46, files[counter].Length-50);
            }
            load_pu.list.labels = list_labels;
            load_timer = 0;
        }
    }

    public void prepareSaving()
    {
        logic.keyboard_disabled = true;
#if UNITY_ANDROID
        keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default);
#endif
    }

    public void save ()
    {
#if UNITY_ANDROID
        StreamWriter myWriter = new StreamWriter("/storage/emulated/0/drone_ui/" + keyboard.text + ".xml");
#else
        InstantGuiInputText save_text = GameObject.Find("save_text").GetComponent<InstantGuiInputText>();
        string name = save_text.text;
        StreamWriter myWriter = new StreamWriter("C:/Users/sebastian/Desktop/settings/waypoints/" + name + ".xml");
#endif
        mySerializer.Serialize(myWriter, waypoints);
        myWriter.Close();

#if !UNITY_ANDROID
        save_text.text = "";
#endif

        GameObject.Find("save_text").SetActive(false);
        GameObject.Find("save_btn").SetActive(false);
        GameObject.Find("cancel_btn").SetActive(false);

        logic.keyboard_disabled = false;
    }

    public void cancel()
    {
        logic.keyboard_disabled = false;
    }

    public void load ()
    {
#if UNITY_ANDROID
        FileStream myFileStream = new FileStream("/storage/emulated/0/drone_ui/" + load_pu.list.labels[load_pu.list.selected] + ".xml", FileMode.Open);
#else
        FileStream myFileStream = new FileStream("C:/Users/sebastian/Desktop/settings/waypoints/" + load_pu.list.labels[load_pu.list.selected] + ".xml", FileMode.Open);

#endif

        waypoints = (List<Vector3>)mySerializer.Deserialize(myFileStream);
    }

    public void resetMap()
    {
        waypoints.Clear();
    }

    public void sendWaypoints()
    {
        status.switch_state(3);
        waypoints_thread = new Thread(
            new ThreadStart(sendWaypointsThread));

        waypoints_thread.IsBackground = true;
        waypoints_thread.Start();
    }

    private void sendWaypointsThread()
    {

        int wp_count = waypoints.Count;
        int sp_count = SEND_RATE * (int)duration;

        float[] x = new float[wp_count + 1];
        float[] y = new float[wp_count + 1];
        float[] z = new float[wp_count + 1];
        int index = 1;

        x[0] = roscomm.packet_recv.t_x;
        y[0] = roscomm.packet_recv.t_y;
        z[0] = roscomm.packet_recv.t_z;

        foreach (Vector3 point in waypoints)
        {
            x[index] = point.x;
            y[index] = point.y;
            z[index] = point.z;
            index++;
        }

        float[] xs, ys, zs;
        TestMySpline.CubicSpline.FitParametric3(x, y, z, sp_count, out xs, out ys, out zs);

        for (int i = 0; i < sp_count; i++)
        {
            roscomm.packet_send.t_x = xs[i] / 100;
            roscomm.packet_send.t_y = ys[i] / 100;
            roscomm.packet_send.t_z = zs[i] / 100;
            waypoints.RemoveAt(i);
            System.Threading.Thread.Sleep(1000 / SEND_RATE);
            print("x: " + xs[i] / 100 + "y: " + ys[i] / 100 + "z:" + zs[i] / 100);
        }
        status.switch_state(2);
    }

}
