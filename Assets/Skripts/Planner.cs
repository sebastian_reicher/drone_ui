﻿using UnityEngine;
using System.Collections;

public class Planner : MonoBehaviour {

    public InstantGuiElement label;
    private int idx = 1; 

	void Start ()
    {
        label = GameObject.Find("planner_idx").GetComponent<InstantGuiElement>();
    }
	
	void Update ()
    {
	
	}

    void setPlannerIdx()
    {
        label.disabled = false;
        label.text = idx.ToString();
        idx++;
    }
}
