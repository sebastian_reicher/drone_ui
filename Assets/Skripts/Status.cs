﻿using UnityEngine;
using System.Collections;

public class Status : MonoBehaviour {

    InstantGuiElement state;

    InstantGuiElement acc_x;
    InstantGuiElement acc_y;
    InstantGuiElement acc_z;
    InstantGuiElement gyr_x;
    InstantGuiElement gyr_y;
    InstantGuiElement gyr_z;
    InstantGuiElement mag_x;
    InstantGuiElement mag_y;
    InstantGuiElement mag_z;
    InstantGuiElement pos_x;
    InstantGuiElement pos_y;
    InstantGuiElement pos_z;
    InstantGuiElement roll;
    InstantGuiElement pitch;
    InstantGuiElement yaw;

    InstantGuiSlider battery;
    InstantGuiSlider wifi;

    RosCommSandbox ros_comm;
    Quaternion rotation;

    public int state_id = 0;

    void Start () {
        state = GameObject.Find("state").GetComponent<InstantGuiElement>();

        battery = GameObject.Find("battery").GetComponent<InstantGuiSlider>();
        wifi = GameObject.Find("wifi").GetComponent<InstantGuiSlider>();

        acc_x = GameObject.Find("acc_x").GetComponent<InstantGuiElement>();
        acc_y = GameObject.Find("acc_y").GetComponent<InstantGuiElement>();
        acc_z = GameObject.Find("acc_z").GetComponent<InstantGuiElement>();
        gyr_x = GameObject.Find("gyr_x").GetComponent<InstantGuiElement>();
        gyr_y = GameObject.Find("gyr_y").GetComponent<InstantGuiElement>();
        gyr_z = GameObject.Find("gyr_z").GetComponent<InstantGuiElement>();
        mag_x = GameObject.Find("mag_x").GetComponent<InstantGuiElement>();
        mag_y = GameObject.Find("mag_y").GetComponent<InstantGuiElement>();
        mag_z = GameObject.Find("mag_z").GetComponent<InstantGuiElement>();
        pos_x = GameObject.Find("pos_x").GetComponent<InstantGuiElement>();
        pos_y = GameObject.Find("pos_y").GetComponent<InstantGuiElement>();
        pos_z = GameObject.Find("pos_z").GetComponent<InstantGuiElement>();
        roll = GameObject.Find("roll").GetComponent<InstantGuiElement>();
        pitch = GameObject.Find("pitch").GetComponent<InstantGuiElement>();
        yaw = GameObject.Find("yaw").GetComponent<InstantGuiElement>();

        ros_comm = GameObject.Find("Main Camera").GetComponent<RosCommSandbox>();

        switch_state(0);
    }

    void Update () {

        battery.shownValue = 1f / (1f - ros_comm.packet_status.battery/100f);
        wifi.shownValue = 1f / (1f - ros_comm.packet_status.wifi/100f);

        acc_x.text = ros_comm.packet_status.acc_x.ToString("n4");
        acc_y.text = ros_comm.packet_status.acc_y.ToString("n4");
        acc_z.text = ros_comm.packet_status.acc_z.ToString("n4");

        gyr_x.text = ros_comm.packet_status.gyr_x.ToString("n4");
        gyr_y.text = ros_comm.packet_status.gyr_y.ToString("n4");
        gyr_z.text = ros_comm.packet_status.gyr_z.ToString("n4");

        mag_x.text = ros_comm.packet_status.mag_x.ToString("n4");
        mag_y.text = ros_comm.packet_status.mag_y.ToString("n4");
        mag_z.text = ros_comm.packet_status.mag_z.ToString("n4");

        pos_x.text = ros_comm.packet_recv.t_x.ToString("n3");
        pos_y.text = ros_comm.packet_recv.t_y.ToString("n3");
        pos_z.text = ros_comm.packet_recv.t_z.ToString("n3");

        rotation = new Quaternion(ros_comm.packet_recv.q_x, -ros_comm.packet_recv.q_y, ros_comm.packet_recv.q_z, ros_comm.packet_recv.q_w);
       
        rotation = Quaternion.Inverse(rotation);
        rotation *= Quaternion.Euler(0, 0, 90);

        if (rotation.eulerAngles.x < 180)
        {
            roll.text = (-rotation.eulerAngles.x).ToString("n1");
        }
        else
        {
            roll.text = (360 - rotation.eulerAngles.x).ToString("n1");
        }

        if (rotation.eulerAngles.y < 180)
        {
            pitch.text = rotation.eulerAngles.y.ToString("n1");
        }
        else
        {
            pitch.text = (rotation.eulerAngles.y - 360).ToString("n1");
        }

        if(rotation.eulerAngles.z < 270)
        {
            yaw.text = (90 - rotation.eulerAngles.z).ToString("n1");
        }
        else
        {
            yaw.text = (rotation.eulerAngles.z - 90).ToString("n1");
        }
        
    }

    public void switch_state (int index)
    {
        switch (index)
        {
            case 0:
                state_id = 0;
                state.text = "LANDED";
                state.style.main.textColor = Color.grey;
                break;
            case 1:
                state_id = 1;
                state.text = "TAKING OFF";
                state.style.main.textColor = Color.yellow;
                break;
            case 2:
                state_id = 2;
                state.text = "HOVERING";
                state.style.main.textColor = Color.green;
                break;
            case 3:
                state_id = 3;
                state.text = "TRAJECTORY";
                state.style.main.textColor = Color.magenta;
                break;
            case 4:
                state_id = 4;
                state.text = "TOUCHING DOWN";
                state.style.main.textColor = Color.yellow;
                break;
            default:
                state_id = -1;
                state.text = "UNDEFINED";
                state.style.main.textColor = Color.red;
                break;
        }

    }
    
}
