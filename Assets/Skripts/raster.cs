﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using System.Threading;

public class raster : MonoBehaviour {

    private const int LOAD_TIME = 30;
    private static int load_timer = 0;

    private Logic logic;
    private InstantGuiPopup load_pu;
    private RosCommSandbox roscomm;

    private InstantGuiButton[,] raster_array;

    private XmlSerializer mySerializer;
    private Thread laserThread;

    private static float counter = 1;

    void Start () {
        raster_array = new InstantGuiButton[5,5];
        logic = GameObject.Find("Main Camera").GetComponent<Logic>();
        load_pu = GameObject.Find("load_laser_pu").GetComponent<InstantGuiPopup>();
        mySerializer = new XmlSerializer(typeof(int));
        roscomm = GameObject.Find("Main Camera").GetComponent<RosCommSandbox>();
        for (int row = 0; row < 5; row++)
        {
            for (int col = 0; col < 5; col++)
            {
                raster_array[row, col] = GameObject.Find("raster_" + row.ToString() + col.ToString()).GetComponent<InstantGuiButton>();
            }
        }
    }
	
	void Update ()
    {
        load_timer++;
        if (load_timer == LOAD_TIME)
        {
            string[] list_labels = load_pu.list.labels;
            string[] files = Directory.GetFiles("C:/Users/sebastian/Desktop/settings/laser");
            int length = files.Length;
            list_labels = new string[length];

            for (int counter = 0; counter < length; counter++)
            {
                list_labels[counter] = files[counter].Substring(42, files[counter].Length - 46);
            }
            load_pu.list.labels = list_labels;
            load_timer = 0;
        }
    }

    void triggerRaster()
    {
        InstantGuiButton raster_element = this.gameObject.GetComponent<InstantGuiButton>();
        int row = Int32.Parse(raster_element.name.Substring(7, 1));
        int col = Int32.Parse(raster_element.name.Substring(8, 1));
        if (raster_array[row, col].text != "0")
        {          
            raster_element.style.main.enabled = false;
            refreshRaster(Int32.Parse(raster_array[row,col].text));
            counter--;
        }
        else
        {
            raster_element.style.main.enabled = true;
            raster_element.text = counter.ToString();
            raster_array[Int32.Parse(raster_element.name.Substring(7, 1)),Int32.Parse(raster_element.name.Substring(8, 1))].text = counter++.ToString();
        }
    }

    public void refreshRaster()
    {
        for (int row = 0; row < 5; row++)
        {
            for (int col = 0; col < 5; col++)
            {
                raster_array[row, col].text = logic.raster[row, col].ToString();
                if(logic.raster[row, col] == 0)
                {
                    raster_array[row, col].style.main.enabled = false;
                }
            }
        }
    }

    public void refreshRaster(int deleted)
    {
        for (int row = 0; row < 5; row++)
        {
            for (int col = 0; col < 5; col++)
            {
                if (raster_array[row, col].text == "0")
                {
                    continue;
                }
                float temp = Int32.Parse(raster_array[row, col].text);
                if (temp > deleted)
                {
                    raster_array[row, col].text = (--temp).ToString();
                    continue;
                }
                if(temp == deleted)
                {
                    raster_array[row, col].text = "0";
                    continue;
                }
            }
        }
    }

    public void saveLaser()
    {
        print("SAVE");
        InstantGuiElement save_text = GameObject.Find("save_text").GetComponent<InstantGuiElement>();
        string name = save_text.text;
        StreamWriter myWriter = new StreamWriter("C:/Users/sebastian/Desktop/settings/laser/" + name + ".xml");
        mySerializer.Serialize(myWriter, logic.raster);
        myWriter.Close();

        save_text.text = "";

        GameObject.Find("save_text").SetActive(false);
        GameObject.Find("save_btn").SetActive(false);
        GameObject.Find("cancel_btn").SetActive(false);
    }

    public void loadLaser()
    {
        print("LOAD");

        FileStream myFileStream = new FileStream("C:/Users/sebastian/Desktop/settings/laser/" + load_pu.list.labels[load_pu.list.selected] + ".xml", FileMode.Open);

        logic.raster = (float[,])mySerializer.Deserialize(myFileStream);
    }

    public void resetLaser()
    {
        counter = 1;
        logic.raster = new float[5,5];
        refreshRaster();
    }

    public void sendLaser()
    {
        laserThread = new Thread(
            new ThreadStart(sendLaserThread));

        laserThread.IsBackground = true;
        laserThread.Start();
    }

    private void sendLaserThread()
    {
        List<float> list = new List<float>();
        for (int i = 1; i < counter; i++)
        {
            for (int row = 0; row < 5; row++)
            {
                for (int col = 0; col < 5; col++)
                {
                    if(Int32.Parse(raster_array[row, col].text) == i)
                    {
                        list.Add(col - 2);
                        list.Add(2 - row);
                    }
                }
            }
        }
        for (int i = 0; i < (25 - counter); i++)
        {
            list.Add(0);
            list.Add(0);
        }
        roscomm.packet_laser.x_1 = list[0];
        roscomm.packet_laser.y_1 = list[1];
        roscomm.packet_laser.x_2 = list[2];
        roscomm.packet_laser.y_2 = list[3];
        roscomm.packet_laser.x_3 = list[4];
        roscomm.packet_laser.y_3 = list[5];
        roscomm.packet_laser.x_4 = list[6];
        roscomm.packet_laser.y_4 = list[7];
        roscomm.packet_laser.x_5 = list[8];
        roscomm.packet_laser.y_5 = list[9];
        roscomm.packet_laser.x_6 = list[10];
        roscomm.packet_laser.y_6 = list[11];
        roscomm.packet_laser.x_7 = list[12];
        roscomm.packet_laser.y_7 = list[13];
        roscomm.packet_laser.x_8 = list[14];
        roscomm.packet_laser.y_8 = list[15];
        roscomm.packet_laser.x_9 = list[16];
        roscomm.packet_laser.y_9 = list[17];
        roscomm.packet_laser.x_10 = list[18];
        roscomm.packet_laser.y_10 = list[19];
        roscomm.packet_laser.x_11 = list[20];
        roscomm.packet_laser.y_11 = list[21];
        roscomm.packet_laser.x_12 = list[22];
        roscomm.packet_laser.y_12 = list[23];
        roscomm.packet_laser.x_13 = list[24];
        roscomm.packet_laser.y_13 = list[25];
        roscomm.packet_laser.x_14 = list[26];
        roscomm.packet_laser.y_14 = list[27];
        roscomm.packet_laser.x_15 = list[28];
        roscomm.packet_laser.y_15 = list[29];
        roscomm.packet_laser.x_16 = list[30];
        roscomm.packet_laser.y_16 = list[31];
        roscomm.packet_laser.x_17 = list[32];
        roscomm.packet_laser.y_17 = list[33];
        roscomm.packet_laser.x_18 = list[34];
        roscomm.packet_laser.y_18 = list[35];
        roscomm.packet_laser.x_19 = list[36];
        roscomm.packet_laser.y_19 = list[37];
        roscomm.packet_laser.x_20 = list[38];
        roscomm.packet_laser.y_20 = list[39];
        roscomm.packet_laser.x_21 = list[40];
        roscomm.packet_laser.y_21 = list[41];
        roscomm.packet_laser.x_22 = list[42];
        roscomm.packet_laser.y_22 = list[43];
        roscomm.packet_laser.x_23 = list[44];
        roscomm.packet_laser.y_23 = list[45];
        roscomm.packet_laser.x_24 = list[46];
        roscomm.packet_laser.y_24 = list[47];
        roscomm.packet_laser.x_25 = list[48];
        roscomm.packet_laser.y_25 = list[49];
    }

}
