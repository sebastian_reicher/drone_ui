﻿using UnityEngine;
using System.Collections;

public class custom_view : MonoBehaviour
{
    private static int TRUCK_FACTOR = 10000;

    private int camera_mode;
    private bool mouse_move;
    private Vector3 mouse;
    private Vector3 initialPosition;
    private Quaternion initialRotation;
    private float old_zoom = 0.09f;

    private Logic logic;

    public InstantGuiButton truck;
    public InstantGuiButton pan;
    public GameObject camera_main;
    public GameObject camera_aerial;
    public GameObject camera_profile;
    public InstantGuiSlider zoom;

    void Start()
    {
        logic = GameObject.Find("Main Camera").GetComponent<Logic>();
        camera_mode = 0;
        mouse_move = false;
        initialPosition = camera_main.transform.position;
        initialRotation = camera_main.transform.rotation;
    }

    void OnEnable()
    {
        camera_profile.SetActive(false);
        camera_aerial.SetActive(false);
    }

    void Update()
    {
        if(!Input.GetMouseButton(0))
        {
            mouse_move = false;
        }
        if (mouse_move == true)
        {
            Vector3 new_mouse = Input.mousePosition;

            if (logic.setpoint_mode)
            {

            }
            else
            {
                switch (camera_mode)
                {
                    case 0:
                        //camera_main.transform.position += new Vector3((mouse.y - new_mouse.y) / TRUCK_FACTOR, (mouse.x - new_mouse.x) / TRUCK_FACTOR, 0);
                        camera_main.transform.position = camera_main.transform.TransformPoint(new Vector3((mouse.x - new_mouse.x) / TRUCK_FACTOR, (mouse.y - new_mouse.y) / TRUCK_FACTOR, 0));
                        break;
                    case 1:
                        Vector3 temp = new Vector3(-(mouse.y - new_mouse.y) / 10, (mouse.x - new_mouse.x) / 10, 0);
                        camera_main.transform.Rotate(temp.x, temp.y, temp.z);
                        break;
                    default:
                        break;
                }
            }

            mouse = new_mouse;
        }

        camera_main.transform.position = camera_main.transform.TransformPoint(new Vector3(0, 0, old_zoom - zoom.value));
        old_zoom = zoom.value;
        //camera_main.transform.position += new Vector3(0,0,zoom.value - camera_main.transform.position.z);
    }

    static bool disable()
    {
        GameObject.Find("load_waypoints_pu").SetActive(false);
        return true;
    }

    public void move()
    {
        mouse = Input.mousePosition;
        mouse_move = true;
    }

    public void enableTruck()
    {
        camera_mode = 0;
        truck.style.main = truck.style.active;
        pan.style.main = pan.style.disabled;

    }

    public void enablePan()
    {
        camera_mode = 1;
        pan.style.main = pan.style.active;
        truck.style.main = truck.style.disabled;
    }

    public void resetCamera()
    {
        camera_main.transform.position = initialPosition;
        camera_main.transform.rotation = initialRotation;
        zoom.value = 0.09f;
    }

}
