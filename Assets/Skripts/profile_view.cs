﻿using UnityEngine;
using System.Collections;

public class profile_view : MonoBehaviour
{
    private const int HIGHT_MIN = 350;
    private const int HIGHT_MAX = 850;
    private const int HIGHT_LENGTH = 500;
    private const float MAP_FACTOR = 0.4f;

    private Waypoints waypoints;
    public GameObject camera_main;
    public GameObject camera_aerial;
    public GameObject camera_profile;

    void Start()
    {
        waypoints = GameObject.Find("Main Camera").GetComponent<Waypoints>();
    }


    void OnEnable()
    {       
        camera_aerial.SetActive(false);
        camera_profile.SetActive(true);
    }

    void Update()
    {
        //Vector3 mouse = Input.mousePosition;
        //print("mouse x: " +  mouse.x.ToString() + "  y: " + mouse.y.ToString());
    }

    public void clickProfile()
    {
        Vector3 mouse = Input.mousePosition;

        if (waypoints.waypoints.Count >= 1 && mouse.y > HIGHT_MIN && mouse.y < HIGHT_MAX)
        {
            Vector3 last_wp = waypoints.waypoints[waypoints.waypoints.Count - 1];
            last_wp.z = (mouse.y - HIGHT_MIN) * MAP_FACTOR;
            waypoints.waypoints.RemoveAt(waypoints.waypoints.Count - 1);
            waypoints.waypoints.Add(last_wp);
        }
    }
}
