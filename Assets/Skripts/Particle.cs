﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Particle : MonoBehaviour
{
    private static float PARTICLE_FACTOR = 425000;
    private static int RESOLUTION = 300;

    private Logic logic;
    private Waypoints waypoints;
    private RosCommSandbox roscomm;

    private int currentResolution;
    private ParticleSystem.Particle[] points;
    private ParticleSystem.Particle[] setpoint;

    public float distance = 0;

    void Start()
    {
        logic = GameObject.Find("Main Camera").GetComponent<Logic>();
        waypoints = GameObject.Find("Main Camera").GetComponent<Waypoints>();
        roscomm = GameObject.Find("Main Camera").GetComponent<RosCommSandbox>();

        setpoint = new ParticleSystem.Particle[1];
        setpoint[0].color = new Color(0f, 0f, 256f);
        setpoint[0].size = 0.0001f;
    }

    void Update()
    {
        //TODO: interpolate only at waypoint change

        if (logic.setpoint_mode)
        {
            setpoint[0].position = new Vector3(logic.sp.x / PARTICLE_FACTOR, logic.sp.y / PARTICLE_FACTOR, logic.sp.z / PARTICLE_FACTOR);
            GetComponent<ParticleSystem>().SetParticles(points, 1);
        }
        else
        {
            int wp_count = waypoints.waypoints.Count;

            if (wp_count < 1)
            {
                points = new ParticleSystem.Particle[1];
            }
            else
            {
                int sp_count = wp_count * RESOLUTION;
                points = new ParticleSystem.Particle[wp_count + sp_count + 1];

                float[] x = new float[wp_count + 1];
                float[] y = new float[wp_count + 1];
                float[] z = new float[wp_count + 1];
                int index = 1;

                x[0] = (roscomm.packet_recv.t_x * 100) / PARTICLE_FACTOR;
                y[0] = (-roscomm.packet_recv.t_y * 100) / PARTICLE_FACTOR;
                z[0] = (roscomm.packet_recv.t_z * 100) / PARTICLE_FACTOR;

                foreach (Vector3 point in waypoints.waypoints)
                {
                    x[index] = point.x / PARTICLE_FACTOR;
                    y[index] = -point.y / PARTICLE_FACTOR;
                    z[index] = point.z / PARTICLE_FACTOR;
                    index++;
                }

                float[] xs, ys, zs;
                TestMySpline.CubicSpline.FitParametric3(x, y, z, sp_count, out xs, out ys, out zs);

                float temp = 0;
                int n = xs.Length;
                for (int i = 1; i < n; i++)
                {
                    float dx = xs[i] - xs[i - 1];
                    float dy = ys[i] - ys[i - 1];
                    float dz = zs[i] - zs[i - 1];
                    temp += (float)Math.Sqrt(dx * dx + dy * dy + dz * dz);
                }
                distance = temp * PARTICLE_FACTOR / 100;

                for (int i = 0; i < wp_count; i++)
                {
                    points[i].position = new Vector3(x[i + 1], y[i + 1], z[i + 1]);
                    points[i].color = new Color(0f, 0f, 0f);
                    points[i].size = 0.00005f;
                }


                for (int i = 0; i < sp_count; i++)
                {
                    points[wp_count + i].position = new Vector3(xs[i], ys[i], zs[i]);
                    points[wp_count + i].color = new Color(i * (1f / (float)sp_count), 1f - i * (1 / (float)sp_count), 0f);
                    points[wp_count + i].size = 0.00002f;
                }
            }

            GetComponent<ParticleSystem>().SetParticles(points, points.Length);
        }

    }
}