﻿using UnityEngine;
using System.Collections;

using System;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Linq;
//using System.Drawing;

using System.Runtime.Serialization.Formatters.Binary;



public class RosCommSandbox : MonoBehaviour
{
    [Serializable()]
    public class RosDataPacket
    {
        public float t_x;
        public float t_y;
        public float t_z;

        public float q_w;
        public float q_x;
        public float q_y;
        public float q_z;
    };

    [Serializable()]
    public class StatusDataPacket
    {
        public float acc_x;
        public float acc_y;
        public float acc_z;

        public float gyr_x;
        public float gyr_y;
        public float gyr_z;

        public float mag_w;
        public float mag_x;
        public float mag_y;
        public float mag_z;

        public float battery;
        public float wifi;
    };

    [Serializable()]
    public class LaserDataPacket
    {
        public float length;
        public float x_1;
        public float y_1;
        public float x_2;
        public float y_2;
        public float x_3;
        public float y_3;
        public float x_4;
        public float y_4;
        public float x_5;
        public float y_5;
        public float x_6;
        public float y_6;
        public float x_7;
        public float y_7;
        public float x_8;
        public float y_8;
        public float x_9;
        public float y_9;
        public float x_10;
        public float y_10;
        public float x_11;
        public float y_11;
        public float x_12;
        public float y_12;
        public float x_13;
        public float y_13;
        public float x_14;
        public float y_14;
        public float x_15;
        public float y_15;
        public float x_16;
        public float y_16;
        public float x_17;
        public float y_17;
        public float x_18;
        public float y_18;
        public float x_19;
        public float y_19;
        public float x_20;
        public float y_20;
        public float x_21;
        public float y_21;
        public float x_22;
        public float y_22;
        public float x_23;
        public float y_23;
        public float x_24;
        public float y_24;
        public float x_25;
        public float y_25;
    };

    [Serializable()]
    public class LaserDataPacketOld
    {
        public float laser_symbol;
        public float laser_x;
        public float laser_y;
    }

    [Serializable()]
    public class PlannerDataPacket
    {
        public float msg_idx;
        public float control_mode;

    }

    Thread receiveThread, sendThread;

    private UdpClient clientReceive = new UdpClient();
    private UdpClient clientSend = new UdpClient();
    private IPEndPoint ep_recv;
    private IPEndPoint ep_send;

    public RosDataPacket packet_recv;
    public RosDataPacket packet_send;
    public LaserDataPacket packet_laser;
    public LaserDataPacketOld packet_laser_old;
    public StatusDataPacket packet_status;
    public PlannerDataPacket packet_planner;
    public byte[] data_JPG;

    //static public Image img_buffer;
    static public int PAR_WEBCAM_JPEG_BUFFER=65000;

    // -------------------------------------------------------------------------
    public void Start()
    {
        init();
    }

    // -------------------------------------------------------------------------
    private void init()
    {
        packet_recv = new RosDataPacket();

        receiveThread = new Thread(
            new ThreadStart(ReceiveData));
        receiveThread.IsBackground = true;
        receiveThread.Start();
        print("Receiving Thread started...");

        Thread.Sleep(100);

        sendThread = new Thread(
            new ThreadStart(SendData));
        sendThread.IsBackground = true;
        sendThread.Start();
        print("Sending Thread started...");
        print("");

        packet_recv.t_x = 0.0f;
        packet_recv.t_y = 0.0f;
        packet_recv.t_z = 0.0f;
        packet_recv.q_w = 1.0f;
        packet_recv.q_x = 0.0f;
        packet_recv.q_y = 0.0f;
        packet_recv.q_z = 0.0f;

        packet_send.t_x = 0.0f;
        packet_send.t_y = 0.0f;
        packet_send.t_z = 0.0f;
        packet_send.q_w = 1.0f;
        packet_send.q_x = 0.0f;
        packet_send.q_y = 0.0f;
        packet_send.q_z = 0.0f;


        packet_laser.length = 0;
        packet_laser.x_1 = 0;
        packet_laser.y_1 = 0;
        packet_laser.x_2 = 0;
        packet_laser.y_2 = 0;
        packet_laser.x_3 = 0;
        packet_laser.y_3 = 0;
        packet_laser.x_4 = 0;
        packet_laser.y_4 = 0;
        packet_laser.x_5 = 0;
        packet_laser.y_5 = 0;
        packet_laser.x_6 = 0;
        packet_laser.y_6 = 0;
        packet_laser.x_7 = 0;
        packet_laser.y_7 = 0;
        packet_laser.x_8 = 0;
        packet_laser.y_8 = 0;
        packet_laser.x_9 = 0;
        packet_laser.y_9 = 0;
        packet_laser.x_10 = 0;
        packet_laser.y_10 = 0;
        packet_laser.x_11 = 0;
        packet_laser.y_11 = 0;
        packet_laser.x_12 = 0;
        packet_laser.y_12 = 0;
        packet_laser.x_13 = 0;
        packet_laser.y_13 = 0;
        packet_laser.x_14 = 0;
        packet_laser.y_14 = 0;
        packet_laser.x_15 = 0;
        packet_laser.y_15 = 0;
        packet_laser.x_16 = 0;
        packet_laser.y_16 = 0;
        packet_laser.x_17 = 0;
        packet_laser.y_17 = 0;
        packet_laser.x_18 = 0;
        packet_laser.y_18 = 0;
        packet_laser.x_19 = 0;
        packet_laser.y_19 = 0;
        packet_laser.x_20 = 0;
        packet_laser.y_20 = 0;
        packet_laser.x_21 = 0;
        packet_laser.y_21 = 0;
        packet_laser.x_22 = 0;
        packet_laser.y_22 = 0;
        packet_laser.x_23 = 0;
        packet_laser.y_23 = 0;
        packet_laser.x_24 = 0;
        packet_laser.y_24 = 0;
        packet_laser.x_25 = 0;
        packet_laser.y_25 = 0;

        packet_laser_old.laser_symbol = 0f;
        packet_laser_old.laser_x = 0f;
        packet_laser_old.laser_y = 0f;

        packet_status.acc_x = 0.0000f;
        packet_status.acc_y = 0.0000f;
        packet_status.acc_z = 0.0000f;

        packet_status.gyr_x = 0.0000f;
        packet_status.gyr_y = 0.0000f;
        packet_status.gyr_z = 0.0000f;

        packet_status.mag_w = 0.0000f;
        packet_status.mag_x = 0.0000f;
        packet_status.mag_y = 0.0000f;
        packet_status.mag_z = 0.0000f;

        packet_status.battery = 100f;
        packet_status.wifi = 100f;

        packet_planner.msg_idx = 0f;
        packet_planner.control_mode = 0f;
    }

    // -------------------------------------------------------------------------
    private void ReceiveData()
    {
        try
        {
            int rcvPort = 1234;
            string serverIP = "172.28.0.2";
            ep_recv = new IPEndPoint(IPAddress.Parse(serverIP), rcvPort);

            clientReceive.Connect(ep_recv);
            print("Receive Client Connected...");
            print("");

            // Send Ping Packet
            byte[] ping_packet = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28 };
            clientReceive.Send(ping_packet, 28);
            print("Ping Packet sent (POSE DATA CHANNEL)...");

            while (Thread.CurrentThread.IsAlive)
            {
                byte[] receivedBytes = clientReceive.Receive(ref ep_recv);
                ReadRosDataPacket(receivedBytes);
                print("Received data from: " + ep_recv.ToString());
                print("T x:" + packet_recv.t_x + " y:" + packet_recv.t_y + " z:" + packet_recv.t_z + "R w:" + packet_recv.q_w + " x:" + packet_recv.q_x + " y:" + packet_recv.q_y + " z:" + packet_recv.q_z + "battery: " + packet_status.battery + "wifi: " + packet_status.wifi);
                

                // Send Command POSE! Check for range first!
                if (Math.Abs(packet_send.t_x) < 100.0f && Math.Abs(packet_send.t_y) < 100.0f && Math.Abs(packet_send.t_z) < 100.0f &&
                    Math.Abs(packet_send.q_w) < 100.0f && Math.Abs(packet_send.q_x) < 100.0f && Math.Abs(packet_send.q_y) < 100.0f && Math.Abs(packet_send.q_z) < 100.0f)
                {
                    string str_send =
                    string.Format("{0,7:00.000}", packet_send.t_x) + ";" +
                    string.Format("{0,7:00.000}", packet_send.t_y) + ";" +
                    string.Format("{0,7:00.000}", packet_send.t_z) + ";" +
                    string.Format("{0,7:00.000}", packet_send.q_w) + ";" +
                    string.Format("{0,7:00.000}", packet_send.q_x) + ";" +
                    string.Format("{0,7:00.000}", packet_send.q_y) + ";" +
                    string.Format("{0,7:00.000}", packet_send.q_z) + ";" +
                    string.Format("{0,7:00.000}", packet_laser_old.laser_symbol) + ";" +
                    string.Format("{0,7:00.000}", packet_laser_old.laser_x) + ";" +
                    string.Format("{0,7:00.000}", packet_laser_old.laser_y) + ";" +
                    string.Format("{0,7:00.000}", packet_planner.msg_idx) + ";" +
                    string.Format("{0,7:00.000}", packet_planner.control_mode) + ";" +

                    string.Format("{0,7:00.000}", packet_laser.length) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_1) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_1) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_2) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_2) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_3) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_3) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_4) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_4) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_5) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_5) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_6) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_6) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_7) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_7) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_8) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_8) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_9) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_9) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_10) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_10) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_11) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_11) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_12) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_12) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_13) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_13) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_14) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_14) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_15) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_15) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_16) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_16) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_17) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_17) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_18) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_18) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_19) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_19) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_20) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_20) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_21) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_21) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_22) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_22) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_23) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_23) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_24) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_24) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.x_25) + ";" +
                    string.Format("{0,7:00.000}", packet_laser.y_25) + ";";

                    clientReceive.Send(Encoding.ASCII.GetBytes(str_send), 504);
                }
                else
                {
                    print("Command Pose Values out of range!");
                }
            }

        }
        catch (Exception e)
        {
            print("Exception: " + e.Message);
        }
    }

    // -------------------------------------------------------------------------
    private void SendData()
    {
        try
        {
            int sendPort = 1235;
            string serverIP = "172.28.0.2";
            ep_send = new IPEndPoint(IPAddress.Parse(serverIP), sendPort);

            clientSend.Connect(ep_send);
            print("Send Client Connected...");
            print("");

            // Send Ping Packet
            byte[] ping_packet = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28 };
            clientSend.Send(ping_packet, 28);
            print("Ping Packet sent (IMAGE CHANNEL)...");


            //MemoryStream ms = new MemoryStream(PAR_WEBCAM_JPEG_BUFFER);
            data_JPG = new byte[PAR_WEBCAM_JPEG_BUFFER];
            //Texture2D texTmp = new Texture2D(1024, 1024, TextureFormat.DXT1, false);

            while (Thread.CurrentThread.IsAlive)
            {
                //Receive Image
                print("Waiting for receive...");
                data_JPG = clientSend.Receive(ref ep_send);
                //ms.Write(data_JPG, 0, data_JPG.Length);
                //img_buffer = Image.FromStream(ms);

                //texTmp.LoadImage(data_JPG); // For now directly load it into texture;
                //Graphics.DrawTexture(new Rect(10, 10, 100, 100), texTmp);
                print("IMAGE RECEIVED! Length: "+data_JPG.Length);   
            }

        }
        catch (Exception e)
        {
            print("Exception: " + e.Message);
        }
    }

    // Convert BYTE ARRAY to OBJECT
    private void ReadRosDataPacket(byte[] byte_array)
    {
        MemoryStream stream = new MemoryStream(byte_array);
        BinaryReader reader = new BinaryReader(stream);

        packet_recv.t_x = reader.ReadSingle();
        packet_recv.t_y = reader.ReadSingle();
        packet_recv.t_z = reader.ReadSingle();

        packet_recv.q_w = reader.ReadSingle();
        packet_recv.q_x = reader.ReadSingle();
        packet_recv.q_y = reader.ReadSingle();
        packet_recv.q_z = reader.ReadSingle();

        packet_status.acc_x = reader.ReadSingle();
        packet_status.acc_y = reader.ReadSingle();
        packet_status.acc_z = reader.ReadSingle();

        packet_status.gyr_x = reader.ReadSingle();
        packet_status.gyr_y = reader.ReadSingle();
        packet_status.gyr_z = reader.ReadSingle();

        packet_status.mag_w = reader.ReadSingle();
        packet_status.mag_x = reader.ReadSingle();
        packet_status.mag_y = reader.ReadSingle();
        packet_status.mag_z = reader.ReadSingle();

        packet_status.battery = reader.ReadSingle();
        packet_status.wifi = reader.ReadSingle();
    }
    private byte[] WriteRosDataPacket(object source)
    {
        var formatter = new BinaryFormatter();
        using (var stream = new MemoryStream())
        {
            formatter.Serialize(stream, source);
            return stream.ToArray();
        }
    }
    //public byte[] WriteRosDataPacket(object value, int maxLength)
    //{
    //    int rawsize = Marshal.SizeOf(value);
    //    byte[] rawdata = new byte[rawsize];
    //    GCHandle handle =
    //        GCHandle.Alloc(rawdata,
    //        GCHandleType.Pinned);
    //    Marshal.StructureToPtr(value,
    //        handle.AddrOfPinnedObject(),
    //        false);
    //    handle.Free();
    //    if (maxLength < rawdata.Length)
    //    {
    //        byte[] temp = new byte[maxLength];
    //        Array.Copy(rawdata, temp, maxLength);
    //        return temp;
    //    }
    //    else
    //    {
    //        return rawdata;
    //    }
    //}

    void OnApplicationQuit()
    {
        receiveThread.Abort();
        sendThread.Abort();
        if (clientReceive != null) clientReceive.Close();
        if (clientSend != null) clientSend.Close();
    }

    public bool reachedDest()
    {
        if (Math.Abs(packet_recv.t_x - packet_send.t_x) < 0.1 && Math.Abs(packet_recv.t_y - packet_send.t_y) < 0.1 && Math.Abs(packet_recv.t_z - packet_send.t_z) < 0.1)
            return true;
        else
            return false;
    }
}