﻿using UnityEngine;
using System.Collections;

public class stream : MonoBehaviour
{
    private RosCommSandbox ros_comm;

    private Texture2D tex;

    void Start()
    {
        tex = new Texture2D(2, 2);
        ros_comm = GameObject.Find("Main Camera").GetComponent<RosCommSandbox>();
    }

    void Update()
    {
        tex.LoadImage(ros_comm.data_JPG);
        GetComponent<InstantGuiElement>().style.main.texture = tex;
    }
}
