﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Threading;

public class Logic : MonoBehaviour
{
    private const int SEND_RATE = 20; //msgs per sec
    private const int TAKE_OFF_DUR = 5; //sec
    private const int TOUCH_DOWN_DUR = 5; //sec
    private const float TAKE_OFF_HEIGHT = 0.75f; //m

    private RosCommSandbox ros_comm;
    private Status status;

    private int timer_left;
    private int timer_right;
    private Thread take_off_thread;
    private Thread touch_down_thread;
    private Thread joystick_left_thread;
    private Thread joystick_right_thread;

    public bool keyboard_disabled = false;
    public float[,] raster;
    public bool planner_mode = false;
    public bool setpoint_mode = false;

    public Vector3 sp;

    InstantGuiElement acc_x;
    InstantGuiElement acc_y;
    InstantGuiElement gyr_x;
    InstantGuiElement gyr_y;

    // Use this for initialization
    void Start () {
        ros_comm = GameObject.Find("Main Camera").GetComponent<RosCommSandbox>();
        status = GameObject.Find("info_panel").GetComponent<Status>();
        raster = new float[5, 5];

        acc_x = GameObject.Find("acc_x").GetComponent<InstantGuiElement>();
        acc_y = GameObject.Find("acc_y").GetComponent<InstantGuiElement>();
        gyr_x = GameObject.Find("gyr_x").GetComponent<InstantGuiElement>();
        gyr_y = GameObject.Find("gyr_y").GetComponent<InstantGuiElement>();

        timer_left = 5;
        timer_right = 5;

        sp = new Vector3(0f, 0f, 1f);
    }
	
	// Update is called once per frame
	void Update () {
        //timer_left--;
        //timer_right--;
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.position.x > 90 && touch.position.x < 390 && touch.position.y > 30 && touch.position.y < 330)
            {
                acc_x.text = (touch.position.x - 240).ToString("n0");
                acc_y.text = (touch.position.y - 180).ToString("n0");

                if (touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Ended)
                {
                    gyr_x.text = "0";
                    gyr_y.text = "0";
                }

                //if (timer_left < 0)
                //{
                //    acc_x.text = "0";
                //    acc_y.text = "0";                   
                //}
                //timer_left = 5;
            }
            else if (touch.position.x > 1530 && touch.position.x < 1830 && touch.position.y > 30 && touch.position.y < 330)
            {
                gyr_x.text = (touch.position.x - 1680).ToString("n0");
                gyr_y.text = (touch.position.y - 180).ToString("n0");

                if (touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Ended)
                {
                    gyr_x.text = "0";
                    gyr_y.text = "0";
                }

                //if (timer_left < 0)
                //{
                //    gyr_x.text = "0";
                //    gyr_y.text = "0";
                //}
                //timer_left = 5;
            }

        }
        
        //Vector3 mouse = Input.mousePosition;
        //print("mouse x: " + mouse.x.ToString() + "  y: " + mouse.y.ToString());

        if (keyboard_disabled == true)
        {
            return;
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            up();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            down();
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            rotCcw();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            rotCw();
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            forward();
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            backward();
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            left();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            right();
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            if(status.state_id == 0)
            {
                takeOff();
            }
            if (status.state_id == 2)
            {
                touchDown();
            }
        }
    }

    public void left()
    {
        ros_comm.packet_send.t_y += 0.1f;
    }

    public void right()
    {
        ros_comm.packet_send.t_y -= 0.1f;
    }

    public void forward()
    {
        ros_comm.packet_send.t_x += 0.1f;
    }

    public void backward()
    {
        ros_comm.packet_send.t_x -= 0.1f;
    }

    public void up()
    {
        ros_comm.packet_send.t_z += 0.1f;
    }

    public void down()
    {
        ros_comm.packet_send.t_z -= 0.1f;
    }

    public void rotCcw()
    {
        Quaternion quat = new Quaternion(ros_comm.packet_send.q_x, ros_comm.packet_send.q_y, ros_comm.packet_send.q_z, ros_comm.packet_send.q_w);
        quat *= Quaternion.Euler(0, 0, 24f);
        ros_comm.packet_send.q_x = quat.x;
        ros_comm.packet_send.q_y = quat.y;
        ros_comm.packet_send.q_z = quat.z;
        ros_comm.packet_send.q_w = quat.w;
    }

    public void rotCw()
    {
        Quaternion quat = new Quaternion(ros_comm.packet_send.q_x, ros_comm.packet_send.q_y, ros_comm.packet_send.q_z, ros_comm.packet_send.q_w);
        quat *= Quaternion.Euler(0, 0, -24f);
        ros_comm.packet_send.q_x = quat.x;
        ros_comm.packet_send.q_y = quat.y;
        ros_comm.packet_send.q_z = quat.z;
        ros_comm.packet_send.q_w = quat.w;
    }

    public void takeOff()
    {
        status.switch_state(1);
        ros_comm.packet_send.t_z = 1.0f;
        status.switch_state(2);

    }


    public void touchDown()
    {
        status.switch_state(4);
        ros_comm.packet_send.t_z = 0f;
        status.switch_state(0);
    }

    public void JoystickLeft()
    {
        joystick_left_thread = new Thread(
            new ThreadStart(joystickLeftThread));

        joystick_left_thread.IsBackground = true;
        joystick_left_thread.Start();
    }

    private void joystickLeftThread()
    {
        Vector3 mouse = Input.mousePosition;
        while (mouse.x > 140 && mouse.x < 340 && mouse.y > 80 && mouse.y < 280)
        {
            status.switch_state(1);
            mouse = Input.mousePosition;
        }
        status.switch_state(4);
    }

    public void JoystickRight()
    {
        joystick_right_thread = new Thread(
            new ThreadStart(joystickRightThread));

        joystick_right_thread.IsBackground = true;
        joystick_right_thread.Start();
    }

    private void joystickRightThread()
    {
        Vector3 mouse = Input.mousePosition;
        while (mouse.x > 140 && mouse.x < 340 && mouse.y > 80 && mouse.y < 280)
        {
            status.switch_state(1);
            mouse = Input.mousePosition;
        }
        status.switch_state(4);
    }

    public void trajectory()
    {
        planner_mode = false;
    }

    public void planner()
    {
        planner_mode = true;
    }

    public void setpoint()
    {
        setpoint_mode = true;
    }

    public void waypoint()
    {
        setpoint_mode = false;
    }

}

